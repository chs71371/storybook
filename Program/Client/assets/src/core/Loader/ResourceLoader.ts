const { ccclass, property } = cc._decorator;

@ccclass
export class ResourceLoader {
    private static __instance: ResourceLoader = null;
    public static get Instance(): ResourceLoader {
        if (null == this.__instance) {
            this.__instance = new ResourceLoader();
        }
        return this.__instance;
    }

    public async Load(rAssetURL: string): Promise<cc.Asset> {
        var rPromise = new Promise<cc.Asset>((resolve, reject) => {
            cc.loader.loadRes(rAssetURL, cc.Asset, (error, prefab) => {
                if (error) {
                    cc.error(error.message || error);
                    resolve(null);
                    return;
                }
                if (!(prefab instanceof cc.Asset)) {
                    cc.log('Result should be a prefab !!!');
                    resolve(null);
                    return;
                }
                resolve(prefab);
            });
        });
        return rPromise;
    }


    public async CreatPrefabs(resName: string, cb: Function) {
        var rUrl = "ui/Prefabs/" + resName;
        var rAsset = await ResourceLoader.Instance.Load(rUrl);

        if (rAsset == null) {
            cc.error("Cannot find prefab: " + rUrl);
        }

        if (cb != null) {
            var obj = cc.instantiate(rAsset);
            cb(obj);
        }
    }

    public async CreatSprite(url: string, cb: Function) {
      
        var rAsset = await ResourceLoader.Instance.Load(url);
        
        if (rAsset == null) {
            cc.error("Cannot find sprite: " + url);
        }

        if (cb != null) {
            cb(rAsset);
        }
    }

}