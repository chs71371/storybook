
export class UtilTool {
    public static DesignWidth: number = 720;
    public static DesignHeight: number = 1280;

    public static Lerp(a: number, b: number, t: number): number {
        var lerpValue = (b - a) * t;


        return a + lerpValue;
    }



    public static Clamp(value: number, min: number, max: number): number {
        var newValue = Math.min(max, value);
        return Math.max(newValue, min);
    }

    public static async Wait(fWaitTime: number) {
        return new Promise((resolve, reject) => {
            setTimeout(() => { resolve(true); }, (fWaitTime));
        });
    }

    public static Random(min: number, max: number):number {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    public static GetWidthRatio() {
        return cc.winSize.width / UtilTool.DesignWidth;
    }

    public static GetHeightRatio() {
        return cc.winSize.height / UtilTool.DesignHeight;
    }

    //用于生成uuid
    private static S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    public static guid(): string {
        return (UtilTool.S4() + UtilTool.S4() + "-" + UtilTool.S4() + "-" + UtilTool.S4() + "-" + UtilTool.S4() + "-" + UtilTool.S4() + UtilTool.S4() + UtilTool.S4());
    }



    public static Orderby<TItem, TValue>(rArray: TItem[], rSelector: (i: TItem) => TValue): TItem[] {
        if (rArray.length <= 1) {
            return rArray;
        }
        const pivotIndex = Math.floor(rArray.length / 2);
        const pivot = rArray.splice(pivotIndex, 1)[0];
        const left = [] as TItem[];
        const right = [] as TItem[];
        for (const i of rArray) {
            if (rSelector(i) < rSelector(pivot)) {
                left.push(i);
            }
            else {
                right.push(i);
            }
        }
        return UtilTool.Orderby(left, rSelector).concat([pivot], UtilTool.Orderby(right, rSelector));
    }

    public static FromUTF8String(str: string) {
        var idx = 0;
        var len = str.length;
        var bytes = [];
        while (idx < len) {
            var c = str.charCodeAt(idx++);
            var buf = [];
            if (c <= 0x7f) {
                // 0XXX XXXX 1 byte
                buf[0] = c;
                buf.length = 1;
            }
            else if (c <= 0x7ff) {
                // 110X XXXX 2 bytes
                buf[0] = (0xc0 | (c >> 6));
                buf[1] = (0x80 | (c & 0x3f));
                buf.length = 2;
            }
            else if (c <= 0xffff) {
                // 1110 XXXX 3 bytes
                buf[0] = (0xe0 | (c >> 12));
                buf[1] = (0x80 | ((c >> 6) & 0x3f));
                buf[2] = (0x80 | (c & 0x3f));
                buf.length = 3;
            }
            [].push.apply(bytes, buf);
        }
        return bytes;
    }

    public static ToUTF8String(bytes) {
        var buf = [];
        var idx = 0;
        var len = bytes.length;
        while (idx < len) {
            var c = bytes[idx++];
            if ((c & 0x80) == 0) {
                // 0XXX XXXX 1 byte (0x00 ~ 0x7f)
                buf.push(c);
            }
            else if ((c & 0xe0) == 0xc0) {
                // 110X XXXX 2 bytes (0xc2 ~ 0xdf)
                var d = bytes[idx++];
                buf.push(((c & 0x1f) << 6) | (d & 0x3f));
            }
            else if ((c & 0xf0) == 0xe0) {
                // 1110 XXXX 3 bytes (0xe0 ~ 0xe1, 0xee ~ 0xef)
                var d = bytes[idx++];
                var e = bytes[idx++];
                buf.push(((c & 0x0f) << 12) | ((d & 0x3f) << 6) | (e & 0x3f));
            }
            else if ((c & 0xf8) == 0xf0) {
                // 1111 0XXX 4 bytes (0xf0 ~ 0xf4)
                var d = bytes[idx++];
                var e = bytes[idx++];
                var f = bytes[idx++];
                buf.push(((c & 0x0f) << 18) | ((d & 0x3f) << 12) |
                    ((e & 0x3f) << 6) | (f & 0x3f));
            }
        }
        return String.fromCharCode.apply(null, buf);
    }
}
