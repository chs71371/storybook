import GameData from "./GameData";
import BattleManage from "./BattleManage";
import { RofCardConfigRow, RofCardEffectConfigRow } from "./ROF/RofElements";
import { RofManager } from "./ROF/RofManager";
import { MissionData } from "./DataRecord";

export enum EffectType {
    //参数改变类型
    HpChange,       //血量变化
    ExpChange,      //经验变化
    MoneyChange,    //金钱变化
    PropChange,     //道具变化

    //角色状态改变
    Invisibility,       //隐身
    HpImmunity,           //伤害免疫
    Invincibility,         //无敌，免疫所有伤害


}

export enum CalcMethod {
    AddSubstract,
    MultiplyDivide,
    Other,
}

export class CardEffect {

    public Config: RofCardEffectConfigRow;

    public CurRound: number;

    public constructor(id: any) {
        this.Config = RofManager.Singleton.CardEffectTable.GetDataByID(id);
        this.CurRound = this.Config.GetRoundCount();
    }
}


export enum CardType {

    //通常卡片
    Normal = 0,
    //抉择卡片
    Choice,
    //描述卡片  
    Describe,
    //过场卡片
    Cinematics,
    //退出卡片
    Back,
    //游戏结束卡片
    Over,

    //战斗
    Battle,

    //待测试
    Action,
}

export class CardData {

    public Config: RofCardConfigRow;

    //同意效果组
    public AgreeEffectArr: CardEffect[] = [];

    //反对效果组
    public OppositionEffectArr: CardEffect[] = [];

    //使用的回合时间记录
    public UseTimeRecord: number = 0;

    //触发任务组
    public Missions: number[] = [];

    //触发成就组
    public Achievements: number[] = [];

    //同意连锁卡牌
    public AgreeChainCard: number = 0;

    //反对连锁卡牌
    public OppoChainCard: number = 0;



    public constructor(id: any) {

        this.Config = RofManager.Singleton.CardTable.GetDataByID(id);
        this.AgreeEffectArr = this.GetCardEffectArr(this.Config.GetAgreeEffectArr());
        this.OppositionEffectArr = this.GetCardEffectArr(this.Config.GetOppositionEffectArr());
        this.Missions = this.Parse(this.Config.GetMissionID());
        this.Achievements = this.Parse(this.Config.GetAchievements());

        var chainArr = this.Config.GetChainCard().split('|');

        if (chainArr.length == 2) {
            this.AgreeChainCard = parseInt(chainArr[0]);
            this.OppoChainCard = parseInt(chainArr[1]);
        }
    }

    private GetCardEffectArr(str: string): CardEffect[] {
        var effectStrArr = str.split('|');
        var effectArr = [];

        effectStrArr.forEach(element => {
            if (element != "0") {
                effectArr.push(new CardEffect(element));
            }
        });

        return effectArr;
    }

    private Parse(list: string): number[] {
        let strList = list.split('|');
        var numberList = [];

        strList.forEach(element => {
            numberList.push(Number(element))
        });

        return numberList;
    }
    
    public IsCanUse(curTime: number): boolean {

        if (this.UseTimeRecord == 0||this.Config.GetCoolingMaxTime()) {
            return true;
        }

        if (curTime - this.UseTimeRecord > this.Config.GetCoolingMaxTime()) {
            return true;

        } else {

            return false;
        }
    }

    public UseCard() {
        this.UseTimeRecord = GameData.Instance.CurTime;
    }

}
