import { RofManager } from "./ROF/RofManager";
import { RofMissionsRow, RofAchievementsRow } from "./ROF/RofElements";
import GameData from "./GameData";

enum ResourceType {
    rtHp,              //血量回复
    rtMoney,           //金钱
    rtGrowth,          //成长值
    rtProp,            //道具
    rtCard,            //卡片触发
}

class ResourceData {
    public Type : ResourceType;
    public Id : number;
    public Value : number;

    constructor(type : ResourceType = ResourceType.rtProp, id : number = 0, value : number = 0) {
        this.Type = type;
        this.Id = id;
        this.Value = value;
    }
}

export enum MissionStatus {
    msUndo, 
    msDoing,
    msDone,
    msWrong,
}

/**
 * 任务类
 */
export class MissionData {
    public Data : RofMissionsRow;
    //奖励组
    public Rewards : ResourceData[] = [];
    //任务完成条件组
    public Conditions : ResourceData[] = [];
    //任务状态
    public Status : MissionStatus = MissionStatus.msUndo;
    //解锁卡组
    public UnlockDecks : number[] = [];

    constructor(id : any) {
        this.Data = RofManager.Singleton.MissionsTable.GetDataByID(id);
        this.Rewards = MissionData.Parse(this.Data.GetRewards());
        this.Conditions = MissionData.Parse(this.Data.GetCondition());
        this.UnlockDecks = MissionData.DeckParse(this.Data.GetUnlockDecks());
    }

    public static Parse(list : string) : ResourceData[] {
        let resourceList : ResourceData[] = [];
        if ("" == list.trim()) {
            return resourceList;
        }
        let strResource = list.split('|');
        strResource.forEach(element => {
            let strPair = element.split(':');
            if (strPair.length < 2) {
                return  //退出遍历
            }

            let resource = new ResourceData();

            //数值
            resource.Value = Number(strPair[1]);
            let keyValue = strPair[0].split('-');
            if (keyValue.length < 2) {
                return      //退出遍历
            }
            resource.Id = Number(keyValue[1]);            
            resource.Type = Number(keyValue[0]);

            resourceList.push(resource);
        });

        return resourceList;
    }

    /**
     * 卡组ID解析
     */
    public static DeckParse(list : string) : number[] {
        let deckList : number[] = [];
        if ("" == list.trim()) {
            return deckList;
        }

        let decks = list.split('|');
        if (decks.length < 1) {
            return deckList;
        }

        decks.forEach(element => {
            deckList.push(Number[element]);
        });
        
        return deckList
    }
}   

/**
 * 数据记录类，记录任务以及成就
 */
export class DataRecord {
    //正在进行的任务队列
    public DoingMissions : MissionData[] = [];
    //已经完成的任务队列
    public DoneMissions : MissionData[] = [];

    //已经完成的成就列表
    public DoneAchievements : AchievementData[] = [];

    /**
     * 改变任务状态
     */
    public ChangeMissionStatus(id : any) : MissionStatus {
        if (0 == id || undefined == id){
            return MissionStatus.msWrong;
        }

        let status = MissionStatus.msDoing;
        let mission = this.GetMissionByID(id, true);
        
        if (null != mission['data']) {
            //完成任务
            status = MissionStatus.msDone
        } else {
            mission = this.GetMissionByID(id, false);
            if (null != mission['data']) {
                //已经是完成过的任务，不需要再次完成
                return MissionStatus.msWrong;
            }
        }        

        let bRetValue = MissionStatus.msDoing;
        switch (status) {
            case MissionStatus.msDoing:
            {
                //直接添加到正在进行的任务队列即可
                this.DoingMissions.push(new MissionData(id));
                break;
            }
            case MissionStatus.msDone:
            {
                //从正在进行队列删除，插入到完成队列中
                if (null == mission) {
                    bRetValue = MissionStatus.msWrong;
                } else {
                    this.DoneMissions.push(mission['data']);
                    this.DoingMissions.splice(mission['index'], 1);

                    //发放奖励
                    this.DispatchRewards(mission['data'].Rewards);

                    //卡组解锁
                    if (this.Unlock(mission['data'].UnlockDecks)) {
                        //界面处理
                        //Todo:
                    }
                    bRetValue = MissionStatus.msDone;
                }                
                break;
            }
        }

        cc.log(this.DoingMissions);
        cc.log(this.DoneMissions);
        return bRetValue;
    }

    /**
     * 根据ID获取相应任务
     */
    public GetMissionByID(id : any, bDoing : boolean) : Object {
        if (id == 0 || id == undefined) {
            return
        }

        let retValue : MissionData = null;
        let nIndex = 0;
        let missionList : MissionData[] = bDoing ? this.DoingMissions : this.DoneMissions;
        for (; nIndex < missionList.length; nIndex++) {
            const element = missionList[nIndex];
            if (null != element && element.Data.GetID() == id) {
                retValue = element
                break;
            }
        }
        return {data : retValue, index : nIndex};
    }

    /**
     * 根据ID获取成就
     */
    public GetAchievementByID(id : any) : Object {
        if (id == 0 || id == undefined) {
            return
        }

        let retValue : AchievementData = null;
        let nIndex = 0;
        for (; nIndex < this.DoneAchievements.length; nIndex++) {
            const element = this.DoneAchievements[nIndex];
            if (null != element && element.Data.GetID() == id) {
                retValue = element
                break;
            }
        }
        return {data : retValue, index : nIndex};
    }

    /**
     * 完成成就
     */
    public AchievementDone(id : any) : boolean{
        if (0 == id || undefined == id) {
            return false
        }

        let achievement = this.GetAchievementByID(id);
        if (null != achievement['data']){
            cc.log("achienement " + id + "already has done");
            return false;
        }

        let achieve = new AchievementData(id);
        this.DoneAchievements.push(achieve);

        //发放奖励
        this.DispatchRewards(achieve.Rewards);

        //卡组解锁
        if (this.Unlock(achieve.UnlockDecks)) {
            //界面处理
            //Todo:
        }

        return true
    }

    /**
     * 发放奖励
     */
    private DispatchRewards(rewards : ResourceData[]) {
        if (rewards.length <= 0) {
            return
        }

        rewards.forEach(element => {
            switch (element.Type) {
                /*case ResourceType.rtCard : {
                    //解锁卡组
                    //Todo:
                    break;
                }*/
                case ResourceType.rtGrowth : {
                    //成长值增加
                    //Todo:
                    break;
                }
                case ResourceType.rtHp : {
                    //血量回复
                    if (element.Value > GameData.Instance.Player.Current.MaxHp - GameData.Instance.Player.Current.HP) {
                        GameData.Instance.Player.Current.HP = GameData.Instance.Player.Current.MaxHp;      
                    } else {
                        GameData.Instance.Player.Current.HP += element.Value;
                    }                    
                    break;
                }
                case ResourceType.rtMoney : {
                    //金钱增加
                    GameData.Instance.Player.Current.Money += element.Value;
                    break;
                }
                case ResourceType.rtProp : {
                    //道具增加
                    if (element.Id <= 0) {
                        break;
                    }

                    if (element.Value == 0) {
                        break;
                    }

                    GameData.Instance.Player.Current.ChangeItem(element.Id, element.Value);
                }
            }
        });
    }

    /**
     * 卡组解锁
     */
    private Unlock (decks : number[]) : boolean{
        if (decks.length < 1) {
            return false;
        }

        decks.forEach(element => {
            let deck = GameData.Instance.AllDeckDict.Get(element);
            if (null != deck && !deck.HasUnlocked) {
                deck.HasUnlocked = true;
            }
        });

        return true
    }
}

/**
 * 成就类
 */
export class AchievementData {
    public Data : RofAchievementsRow;

    //奖励组
    public Rewards : ResourceData[] = [];

    //成就完成条件
    public Conditions : ResourceData[] = [];

    //解锁卡组
    public UnlockDecks : number[] = []; 

    //构造函数
    public constructor(id : any) {
        this.Data = RofManager.Singleton.AchievementTable.GetDataByID(id);
        this.Rewards = MissionData.Parse(this.Data.GetRewards());
        this.Conditions = MissionData.Parse(this.Data.GetCondition());
        this.UnlockDecks = MissionData.DeckParse(this.Data.GetUnlockDecks());
    }
}