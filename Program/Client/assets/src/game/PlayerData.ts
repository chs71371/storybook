import { ItemData, ItemType } from "./ItemData"

export class PlayerBaseData {

    public HP: number = 0;

    public Resource: number = 0;

    public Money: number = 0;

    public Origin: number = 1;

    public Level: number = 1;

    public MaxHp: number = 0;

    public RankLimit: number = 0;

    //拥有的道具列表
    public OwnedItem: ItemData[] = [];

    public constructor(isInitial: boolean = false) {
        if (isInitial) {
            this.HP = 100;
            this.Resource = 0;
            this.Money = 0;
            this.Origin = 5;
            this.Level = 1;
            this.FreshData();
        }
    }

    public FreshData() {
        this.FreshMaxHp();
        this.FreshRankLimit();
    }

    public FreshMaxHp() {
        this.MaxHp = this.Level * 1 + 100;
    }

    public FreshRankLimit() {
        this.RankLimit = this.Level * 1 + 100;
    }

    //添加道具
    public ChangeItem(id: number, count: number) {
        let item = this.GetItemById(id);
        if (null == item) {
            //新增道具  
            if (count < 0) {
                return
            }
            this.OwnedItem.push(new ItemData(id, count));
        } else {
            //数量增减
            item.Count += count;
            //减为0则从道具列表中删除
            if (0 == item.Count) {
                if (!this.DeleteItemById(id)) {
                    cc.log("Delete Prop Failed");
                }
            }
        }
    }

    //根据道具ID获取道具
    public GetItemById(id: number): ItemData {
        let RetValue = null
        this.OwnedItem.forEach((value, index) => {
            if (null != value && value.ID == id) {
                RetValue = value;
                //此处为退出循环，而不是退出函数调用
                return
            }
        });
        return RetValue
    }

    //从道具列表中删除道具
    public DeleteItemById(id: number): boolean {
        if (0 == id || undefined == id) {
            return false
        }

        let item = this.GetItemById(id);

        let nIndex = this.OwnedItem.indexOf(item);
        if (nIndex >= 0) {
            this.OwnedItem.splice(nIndex, 1);
        }

        return true
    }
}


export class CombatData {

    public HP: number = 100;

    public Atk: number = 5;

    public Def: number = 5;
}



export default class PlayerData {


    public Current: PlayerBaseData = null;

    public Inherit: PlayerBaseData = null;


    public Init() {

        this.Current = new PlayerBaseData(true);

        if (this.Inherit != null) {

            this.Current.Origin += this.Inherit.Origin;
        }
    }

    public OnPropertyChange() {
        this.Current.Resource += this.Current.Origin * 1;
    }


}
