

const { ccclass, property } = cc._decorator;

@ccclass
export   class GameObjectPool extends cc.Component {

    @property(cc.Prefab)
    objectPrefab: cc.Prefab = null;

    
    elementPool: cc.Node[] = [];

    public GetObject(): cc.Node {
         return cc.instantiate(this.objectPrefab) as cc.Node;

        if (this.elementPool.length == 0) {
            return cc.instantiate(this.objectPrefab) as cc.Node;
        } else {
            var obj = this.elementPool.pop();
            obj.parent.removeChild(obj);
            obj.active = true;
            return obj;
        }
    }

    public GetObjectToParent(parent: cc.Node): cc.Node {
        var obj = this.GetObject();
        parent.addChild(obj);
        obj.x = 0;
        obj.y = 0;
        return obj;
    }

    public ReturnObject(obj: cc.Node) {
        obj.active = false;
        obj.parent.removeChild(obj);
        this.node.addChild(obj);
        this.elementPool.push(obj);
    }

}
