
import GameMain from "./GameMain";
import Config from "./Config";
import BattleManage from "./BattleManage";
import { CardData, CardType } from "./CardData";
import { ResourceLoader } from "../core/Loader/ResourceLoader";


const { ccclass, property } = cc._decorator;

@ccclass
export default class CardView extends cc.Component {

    @property(cc.Sprite)
    showImage: cc.Sprite = null;

    @property(cc.Button)
    actionButton: cc.Button = null;

    @property(cc.Label)
    chooseDes: cc.Label = null;

    @property(cc.Label)
    cardDesCenter: cc.Label = null;

    @property(cc.Label)
    cardDescribeTitle: cc.Label = null;

    @property(cc.Node)
    imgFloor: cc.Node = null;

    @property(cc.Node)
    animPoint: cc.Node = null;

    protected mViewData: CardViewData;

    protected mMoveSpeed: number = 0.04;

    protected mUseJudgeRot: number = 6;

    protected mIsUse = false;

    protected mIsChoose = false;

    protected mFloolInitY: number = 430;

    protected mFloolShowY: number = 220;

    onLoad() {

    }

    private registerEvent() {

        var self = this;

        this.actionButton.node.on(cc.Node.EventType.TOUCH_START, function (event) {

            self.mIsChoose = true;
            self.animPoint.stopAllActions();
            self.imgFloor.stopAllActions();
        }, this);

        this.actionButton.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            self.TouchCard(event);
        }, this);



        this.actionButton.node.on(cc.Node.EventType.TOUCH_END, function (event) {


            if (self.mIsChoose) {
                self.mIsChoose = false;
                self.TouchCardEnd();
            }
        }, this);

    }



    public GetViewData(): CardViewData {
        return this.mViewData;
    }

    public SetView(data: CardData) {


        this.mViewData = new CardViewData();
        this.mViewData.Data = data;
        this.showImage.node.active = true;
        this.cardDesCenter.node.active = false;
        this.cardDescribeTitle.string = "";

        this.registerEvent();

        switch (this.mViewData.Data.Config.GetType()) {

            case CardType.Describe:
                this.showImage.node.active = false;
                this.cardDesCenter.node.active = true;
                this.cardDesCenter.string = this.mViewData.Data.Config.GetDescribe();
                break;
            case CardType.Normal:
                this.cardDescribeTitle.string = this.mViewData.Data.Config.GetDescribe();
                break;
            case CardType.Battle:
            case CardType.Choice:
            case CardType.Back:
                this.cardDescribeTitle.string = this.mViewData.Data.Config.GetDescribe();
                break;
        }

        if (data.Config.GetShowImgUrl() != "0") {

            ResourceLoader.Instance.CreatSprite(Config.CardPath + data.Config.GetShowImgUrl(), (o) => {

                this.showImage.spriteFrame = new cc.SpriteFrame(o as cc.Texture2D);
                this.mViewData.ContentImage = this.showImage.spriteFrame;
            });
        }

        if (data.Config.GetShowBgUrl() != "0") {
            BattleManage.Instance.battlePanel.SetBgImg(data.Config.GetShowBgUrl());
        }


        this.mIsUse = false;
        this.animPoint.rotation = 0;
        this.imgFloor.rotation = 0;
        this.imgFloor.y = this.mFloolInitY;
    }



    private TouchCard(event) {
        if (this.mIsUse) {
            return;
        }
        var delta = event.touch.getDelta();
        this.animPoint.rotation += delta.x * this.mMoveSpeed;

        this.animPoint.rotation = Math.min(this.mUseJudgeRot, this.animPoint.rotation);
        this.animPoint.rotation = Math.max(-this.mUseJudgeRot, this.animPoint.rotation);

        this.chooseDes.string = "";


        switch (this.mViewData.Data.Config.GetType()) {

            case CardType.Describe:

                break;
            default:
                if (this.animPoint.rotation > 0) {
                    if (this.mViewData.Data.Config.GetAgreeDes() != "0") {
                        this.chooseDes.string = this.mViewData.Data.Config.GetAgreeDes();
                    }
                }

                if (this.animPoint.rotation < 0) {
                    if (this.mViewData.Data.Config.GetAgreeDes() != "0") {
                        this.chooseDes.string = this.mViewData.Data.Config.GetOppositionDes();
                    }
                }

                this.chooseDes.node.x = -this.animPoint.rotation * 10;

                this.imgFloor.y = this.mFloolInitY - Math.abs(this.animPoint.rotation * (this.mFloolInitY - this.mFloolShowY) / 5);
                this.imgFloor.rotation = -this.animPoint.rotation;
                break;
        }


    }

    private TouchCardEnd() {

        if (this.mIsUse) {
            return;
        }


        if (this.animPoint.rotation == this.mUseJudgeRot) {
            this.UseCard(true);
            return;
        }

        if (this.animPoint.rotation == -this.mUseJudgeRot) {

            this.UseCard(false);
            return;
        }

        var action = cc.rotateTo(0.4, 0);

        var spawn = cc.spawn(
            cc.rotateTo(0.2, 0),
            cc.moveTo(0.2, new cc.Vec2(0, this.mFloolInitY))
        )

        this.animPoint.runAction(action);
        this.imgFloor.runAction(spawn);
    }

    private UseCard(isAgree: boolean) {

        this.mIsUse = true;
        this.mViewData.IsAgress = isAgree;
        this.UseCardAnim();

        if (isAgree) {

            this.mViewData.EffectContent = this.mViewData.Data.Config.GetAgreeResultDes();
        } else {

            this.mViewData.EffectContent = this.mViewData.Data.Config.GetOppositionResultDes();
        }

        BattleManage.Instance.battlePanel.OnUseCard();
    }

    private UseCardAnim() {

        var action = cc.rotateTo(1, this.mViewData.IsAgress ? 120 : -120);

        var spawn = cc.moveTo(1, new cc.Vec2(0, -200)).easing(cc.easeExponentialOut());

        var fade = cc.fadeOut(0.3);

        this.animPoint.runAction(action);
        this.node.runAction(spawn);
        this.cardDescribeTitle.node.runAction(fade);
    }

    public OnReturnCard() {
        this.scheduleOnce(() => {

            this.node.destroy();
             }, 1.1);
    }

}



export class CardViewData {
    public ContentImage: cc.SpriteFrame;
    public Data: CardData;
    public IsAgress: boolean;
    public EffectContent: string;

}