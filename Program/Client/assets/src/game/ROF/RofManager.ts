import { ByteBuffer } from "../../Core/Assist/ByteBuffer";
import { Func } from "../../Core/Assist/Func";
import {  RofLanguageRow, RofDeckConfigRow, RofCardConfigRow, RofCardEffectConfigRow, RofPropsRow, RofMissionsRow, RofAchievementsRow, RofMonstersRow } from "./RofElements";


export interface IRofBase {
    ReadBody(rData: ByteBuffer);
}

var arrayBufferHandler = function (item, callback) {
    var url = item.url;
    var xhr = cc.loader.getXMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "arraybuffer";
    xhr.onload = function (oEvent) {
        var arrayBuffer = xhr.response;
        if (arrayBuffer) {
            var result = new Uint8Array(arrayBuffer);
            // 任何需要的处理
            callback(null, result);
        }
    }
    xhr.send(null);
};

export class RofTable<T extends IRofBase>
{
    private mPath: string;
    private mColNum: number;
    private mRowNum: number;
    private mIDMap: { [key: number]: T };
    private mRowMap: { [key: number]: number };

    constructor(private mTemplate: { new(): T; }, strPath: string) {
        RofManager.MaxCount++;
        this.mPath = strPath;
        this.mColNum = 0;
        this.mRowNum = 0;
        this.mIDMap = {};
        this.mRowMap = {};
        let strUrl = cc.url.raw(this.mPath)
        var that = this
        cc.loader.load({ url: strUrl, type: "bytes" }, function (err, rData) {
            if (err) {
                cc.error(err)
                return
            }
            that.Analysis(rData)
        })
    }

    private Analysis(rData: Uint8Array): void {
        let rBuffer = new ByteBuffer(rData.buffer);
        rBuffer.LittleEndian = false;
        rBuffer.Offset = 64;

        this.mRowNum = rBuffer.ReadInt32();
        this.mColNum = rBuffer.ReadInt32();

        //解析头
        for (let i = 0; i < this.mColNum; i++) {
            let nNameLen = rBuffer.ReadUInt8();
            rBuffer.Offset += nNameLen + 2;
        }
        //解析行
        for (let i = 0; i < this.mRowNum; i++) {
            let nID = rBuffer.ReadInt32()
            rBuffer.Offset -= 4;
            let obj = new this.mTemplate();
            obj.ReadBody(rBuffer);
            this.mIDMap[nID] = obj;
            this.mRowMap[i] = nID;
        }
        RofManager.CurCount++;
    }

    public GetData(): { [key: number]: T } {
        return this.mIDMap;
    }

    public GetDataByID(nID: number): T {
        return this.mIDMap[nID];
    }

    public GetDataByRow(nIndex: number): T {
        if (this.mRowMap[nIndex] == null) {
            return null;
        }
        let nID = this.mRowMap[nIndex];
        return this.mIDMap[nID];
    }

    public GetRowNum(): number {
        return this.mRowNum;
    }

    public GetColNum(): number {
        return this.mColNum;
    }

}

export class RofMultiLanguage {
    static GetMultiLanguage(nID: number): string {
        return RofManager.Singleton.LanguageTable.GetDataByID(nID).GetChinese()
    }
}


export class RofManager {
    static CurCount: number = 0;
    static MaxCount: number = 0;
    static mSingleton: RofManager;

    static get Singleton(): RofManager {
        if (this.mSingleton == null) {
            this.mSingleton = new RofManager()
        }
        return this.mSingleton;
    }

    public init(): boolean {
        cc.loader.addLoadHandlers({
            'bytes': arrayBufferHandler
        });
        return true
    }

    
    public readonly LanguageTable: RofTable<RofLanguageRow> = new RofTable<RofLanguageRow>(RofLanguageRow, "resources/rofs/RofLanguage.bytes");
    public readonly DeckTable: RofTable<RofDeckConfigRow> = new RofTable<RofDeckConfigRow>(RofDeckConfigRow, "resources/rofs/RofDeckConfig.bytes");
    public readonly CardTable: RofTable<RofCardConfigRow> = new RofTable<RofCardConfigRow>(RofCardConfigRow, "resources/rofs/RofCardConfig.bytes");
    public readonly CardEffectTable: RofTable<RofCardEffectConfigRow> = new RofTable<RofCardEffectConfigRow>(RofCardEffectConfigRow, "resources/rofs/RofCardEffectConfig.bytes");
    public readonly PropsTable: RofTable<RofPropsRow> = new RofTable<RofPropsRow>(RofPropsRow, "resources/rofs/RofProps.bytes");
    public readonly MissionsTable: RofTable<RofMissionsRow> = new RofTable<RofMissionsRow>(RofMissionsRow, "resources/rofs/RofMissions.bytes");
    public readonly AchievementTable: RofTable<RofAchievementsRow> = new RofTable<RofAchievementsRow>(RofAchievementsRow, "resources/rofs/RofAchievements.bytes");
    public readonly MonstersTable: RofTable<RofMonstersRow> = new RofTable<RofMonstersRow>(RofMonstersRow, "resources/rofs/RofMonsters.bytes");
}

