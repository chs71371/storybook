import { ByteBuffer } from '../../Core/Assist/ByteBuffer';
import { IRofBase, RofMultiLanguage } from './RofManager';
export class RofAchievementsRow implements IRofBase
{
private mID : number = 0;
private mType : number = 0;
private mName : string = "";
private mDescribe : string = "";
private mRewards : string = "";
private mCondition : string = "";
private mUnlockDecks : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mType = rData.ReadInt32();
this.mName= rData.ReadString();
this.mDescribe= rData.ReadString();
this.mRewards= rData.ReadString();
this.mCondition= rData.ReadString();
this.mUnlockDecks= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetType() : number { return this.mType }
public GetName() : string { return this.mName }
public GetDescribe() : string { return this.mDescribe }
public GetRewards() : string { return this.mRewards }
public GetCondition() : string { return this.mCondition }
public GetUnlockDecks() : string { return this.mUnlockDecks }
}
export class RofCardConfigRow implements IRofBase
{
private mID : number = 0;
private mType : number = 0;
private mChainCard : string = "";
private mCoolingMaxTime : number = 0;
private mRandomValue : number = 0;
private mDescribe : string = "";
private mAgreeDes : string = "";
private mOppositionDes : string = "";
private mAgreeResultDes : string = "";
private mOppositionResultDes : string = "";
private mAgreeEffectArr : string = "";
private mOppositionEffectArr : string = "";
private mShowImgUrl : string = "";
private mShowBgUrl : string = "";
private mChainDeck : string = "";
private mMissionID : string = "";
private mAchievements : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mType = rData.ReadInt32();
this.mChainCard= rData.ReadString();
this.mCoolingMaxTime = rData.ReadInt32();
this.mRandomValue = rData.ReadFloat32();
this.mDescribe= rData.ReadString();
this.mAgreeDes= rData.ReadString();
this.mOppositionDes= rData.ReadString();
this.mAgreeResultDes= rData.ReadString();
this.mOppositionResultDes= rData.ReadString();
this.mAgreeEffectArr= rData.ReadString();
this.mOppositionEffectArr= rData.ReadString();
this.mShowImgUrl= rData.ReadString();
this.mShowBgUrl= rData.ReadString();
this.mChainDeck= rData.ReadString();
this.mMissionID= rData.ReadString();
this.mAchievements= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetType() : number { return this.mType }
public GetChainCard() : string { return this.mChainCard }
public GetCoolingMaxTime() : number { return this.mCoolingMaxTime }
public GetRandomValue() : number { return this.mRandomValue }
public GetDescribe() : string { return this.mDescribe }
public GetAgreeDes() : string { return this.mAgreeDes }
public GetOppositionDes() : string { return this.mOppositionDes }
public GetAgreeResultDes() : string { return this.mAgreeResultDes }
public GetOppositionResultDes() : string { return this.mOppositionResultDes }
public GetAgreeEffectArr() : string { return this.mAgreeEffectArr }
public GetOppositionEffectArr() : string { return this.mOppositionEffectArr }
public GetShowImgUrl() : string { return this.mShowImgUrl }
public GetShowBgUrl() : string { return this.mShowBgUrl }
public GetChainDeck() : string { return this.mChainDeck }
public GetMissionID() : string { return this.mMissionID }
public GetAchievements() : string { return this.mAchievements }
}
export class RofCardEffectConfigRow implements IRofBase
{
private mID : number = 0;
private mType : number = 0;
private mValue : string = "";
private mCalculate : number = 0;
private mRoundCount : number = 0;
private mItemID : number = 0;
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mType = rData.ReadInt32();
this.mValue= rData.ReadString();
this.mCalculate = rData.ReadInt32();
this.mRoundCount = rData.ReadInt32();
this.mItemID = rData.ReadInt32();
}
public GetID() : number { return this.mID }
public GetType() : number { return this.mType }
public GetValue() : string { return this.mValue }
public GetCalculate() : number { return this.mCalculate }
public GetRoundCount() : number { return this.mRoundCount }
public GetItemID() : number { return this.mItemID }
}
export class RofDeckConfigRow implements IRofBase
{
private mID : number = 0;
private mStartupCardId : number = 0;
private mRandomEventPool : string = "";
private mLimitCount : number = 0;
private mOverDeck : number = 0;
private mUnlockCond : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mStartupCardId = rData.ReadInt32();
this.mRandomEventPool= rData.ReadString();
this.mLimitCount = rData.ReadInt32();
this.mOverDeck = rData.ReadInt32();
this.mUnlockCond= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetStartupCardId() : number { return this.mStartupCardId }
public GetRandomEventPool() : string { return this.mRandomEventPool }
public GetLimitCount() : number { return this.mLimitCount }
public GetOverDeck() : number { return this.mOverDeck }
public GetUnlockCond() : string { return this.mUnlockCond }
}
export class RofLanguageRow implements IRofBase
{
private mID : number = 0;
private mChinese : string = "";
private mEnglish : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mChinese= rData.ReadString();
this.mEnglish= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetChinese() : string { return this.mChinese }
public GetEnglish() : string { return this.mEnglish }
}
export class RofMissionsRow implements IRofBase
{
private mID : number = 0;
private mType : number = 0;
private mName : string = "";
private mDescribe : string = "";
private mRewards : string = "";
private mCondition : string = "";
private mUnlockDecks : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mType = rData.ReadInt32();
this.mName= rData.ReadString();
this.mDescribe= rData.ReadString();
this.mRewards= rData.ReadString();
this.mCondition= rData.ReadString();
this.mUnlockDecks= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetType() : number { return this.mType }
public GetName() : string { return this.mName }
public GetDescribe() : string { return this.mDescribe }
public GetRewards() : string { return this.mRewards }
public GetCondition() : string { return this.mCondition }
public GetUnlockDecks() : string { return this.mUnlockDecks }
}
export class RofMonstersRow implements IRofBase
{
private mID : number = 0;
private mType : number = 0;
private mName : string = "";
private mDescribe : string = "";
private mHP : number = 0;
private mAttack : number = 0;
private mDefense : number = 0;
private mExp : number = 0;
private mProps : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mType = rData.ReadInt32();
this.mName= rData.ReadString();
this.mDescribe= rData.ReadString();
this.mHP = rData.ReadInt32();
this.mAttack = rData.ReadInt32();
this.mDefense = rData.ReadInt32();
this.mExp = rData.ReadInt32();
this.mProps= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetType() : number { return this.mType }
public GetName() : string { return this.mName }
public GetDescribe() : string { return this.mDescribe }
public GetHP() : number { return this.mHP }
public GetAttack() : number { return this.mAttack }
public GetDefense() : number { return this.mDefense }
public GetExp() : number { return this.mExp }
public GetProps() : string { return this.mProps }
}
export class RofPropsRow implements IRofBase
{
private mID : number = 0;
private mType : number = 0;
private mName : string = "";
private mDescribe : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mType = rData.ReadInt32();
this.mName= rData.ReadString();
this.mDescribe= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetType() : number { return this.mType }
public GetName() : string { return this.mName }
public GetDescribe() : string { return this.mDescribe }
}
