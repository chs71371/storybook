import GameMain from "./GameMain";
import Config from "./Config";
import { GameObjectPool } from "./GameObjectPool";
import CardView from "./CardView";

import GameData from "./GameData";
import { CardData, CardType } from "./CardData";
import { UtilTool } from "../core/Assist/UtilTool";
import BattleManage from "./BattleManage";
import { ResourceLoader } from "../core/Loader/ResourceLoader";



const { ccclass, property } = cc._decorator;

@ccclass
export default class BattlePanel extends cc.Component {

    @property(cc.Node)
    actionPanel: cc.Node = null;

    @property(cc.Node)
    cardPanel: cc.Node = null;

    @property(cc.Node)
    cardElementPoint: cc.Node = null;

    @property(cc.Node)
    cardActionContent: cc.Node = null;

    @property(cc.Component)
    cardObjectPool: GameObjectPool = null;

    //界面切换
    @property(cc.Animation)
    EnterStoryOrCombat: cc.Animation = null;

    //提示
    @property(cc.Animation)
    CardEffectAnimation: cc.Animation = null;

    @property(cc.Label)
    CardEffectContent: cc.Label = null;

    @property(cc.Node)
    CardEffectContineButton: cc.Node = null;


    //背景
    @property(cc.Animation)
    SceneBgAnimation: cc.Animation = null;
    @property(cc.Sprite)
    SceneBgImg: cc.Sprite = null;
    @property(cc.Sprite)
    SceneBgAimImg: cc.Sprite = null;


    //属性
    @property(cc.Label)
    hpNode: cc.Label = null;

    @property(cc.Label)
    speedNode: cc.Label = null;

    @property(cc.Label)
    moneyNode: cc.Label = null;

    @property(cc.Label)
    rescourceNode: cc.Label = null;

    protected mCurShowCard: CardView = null;

    protected mSlideSpeed: number = 5;

    protected mIsUseCard: boolean = false;

    protected mCurBgName: string = "";


    onLoad() {
        this.actionPanel.active = false;
        this.cardPanel.active = false;
    }

    public SwitchStoryOrCombat(isCombat: boolean) {
        if (isCombat) {
            this.EnterStoryOrCombat.play("Battle_Combat_Enter", 0);
        } else {
            this.EnterStoryOrCombat.play("Battle_Combat_Back", 0);

            this.scheduleOnce(() => {
                this.CheckShowEffectContent();
            }, 1);
        }
    }

    public OnActionReady() {
        this.cardPanel.active = false;
        this.actionPanel.active = true;
    }

    public SetBgImg(resName: string) {

        if (resName != this.mCurBgName) {

            this.mCurBgName = resName;

            this.SceneBgAnimation.play("BG_Switch_01", 0);

            this.SceneBgAimImg.spriteFrame = this.SceneBgImg.spriteFrame;

            ResourceLoader.Instance.CreatSprite(Config.BgPath + resName, (o) => {

                this.SceneBgImg.spriteFrame = new cc.SpriteFrame(o as cc.Texture2D);

            });
        }
    }


    public SetCurAction(dataArr: CardData[]) {
        for (let index = 0; index < dataArr.length; index++) {
            const element = dataArr[index];

            var newCard = this.cardObjectPool.GetObjectToParent(this.cardActionContent);
            var cardScript = newCard.getComponent(CardView);
            cardScript.SetView(element);
        }
    }


    public OnBattleReady() {
        this.actionPanel.active = false;
        this.cardPanel.active = true;

        this.FreshCardById(GameData.Instance.CurDeck.Config.GetStartupCardId());
    }

    private SetCurCard(data: CardData) {

        var newCard = this.cardObjectPool.GetObjectToParent(this.cardElementPoint);
        this.mCurShowCard = newCard.getComponent(CardView);
        this.mCurShowCard.SetView(data);
    }


    public FreshCardById(id: number) {
        this.SetCurCard(GameData.Instance.AllCardDict.Get(id));
    }


    public OnUseCard() {
        this.mIsUseCard = true;

        switch (this.mCurShowCard.GetViewData().Data.Config.GetType()) {
            case CardType.Battle:
                if (this.mCurShowCard.GetViewData().IsAgress) {
                    BattleManage.Instance.combatPanel.InitCombat(this.mCurShowCard.GetViewData());
                    this.SwitchStoryOrCombat(true);
                } else {
                    this.OnUseCardOver();
                }
                break;
            default:
                this.CheckShowEffectContent();
                break;
        }
    }

    public OnUseCardOver() {
        this.mIsUseCard = false;
        this.mCurShowCard.OnReturnCard();
        BattleManage.Instance.OnActionUseCardEffect(this.mCurShowCard.GetViewData());
    }


    private CheckShowEffectContent() {
        if (this.mCurShowCard.GetViewData().EffectContent != "0") {
            this.CardEffectContineButton.active = true;
            this.CardEffectContent.string = this.mCurShowCard.GetViewData().EffectContent;
            this.CardEffectAnimation.play("cardEffect_Active", 0);

        } else {
            this.OnUseCardOver();
        }
    }

    public OnContinueGame() {

        if (this.mIsUseCard) {
            this.CardEffectAnimation.play("cardEffect_Disable", 0);
            this.CardEffectContineButton.active = false;
            this.OnUseCardOver();
        }
    }




    public Warn() {
        alert("you died");
    }


    update(dt) {

        if (GameData.Instance.Player == null) {
            return;
        }

        this.hpNode.string = GameData.Instance.Player.Current.HP + "/" + GameData.Instance.Player.Current.MaxHp;

        this.speedNode.string = "" + GameData.Instance.Player.Current.Origin;

        this.moneyNode.string = "" + GameData.Instance.Player.Current.Money;

        this.rescourceNode.string = GameData.Instance.Player.Current.Resource + "/" + GameData.Instance.Player.Current.RankLimit;
    }

}
