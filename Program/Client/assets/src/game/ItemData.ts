import {RofManager} from "./ROF/RofManager";
import { RofPropsRow } from "./ROF/RofElements";

export enum ItemType {
    Key, 
    Drug,    
    Other,
}

export class ItemData {
    public ID : number = 0;
    public Count : number = 0;

    public constructor(id : number, count : number) {
        this.ID = id;
        this.Count = count;
    }
}