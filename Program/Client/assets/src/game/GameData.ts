
import GameMain from "./GameMain";
import Config from "./Config";
import PlayerData from "./PlayerData";
import { CardData, CardEffect, EffectType, CardType, CalcMethod } from "./CardData";
import { DeckData, DeckCardType } from "./DeckData";
import { UtilTool } from "../core/Assist/UtilTool";
import { RofManager } from "./ROF/RofManager";
import { NumberDict } from "../core/Assist/Dict";

export default class GameData {

    private static _Instance: GameData;

    public static get Instance(): GameData {
        if (GameData._Instance == null) {
            GameData._Instance = new GameData();
        }
        return GameData._Instance;
    }

    //玩家属性
    public Player: PlayerData = null;

    //当前进行的时间（卡牌使用次数）
    public CurTime: number = 0;

    //当前卡组
    public CurDeck: DeckData;

    //游戏所有卡牌字典
    public AllCardDict: NumberDict<CardData> = new NumberDict<CardData>();

    //游戏所有卡组字典
    public AllDeckDict: NumberDict<DeckData> = new NumberDict<DeckData>();


    public InitData() {

        this.InitPlayer();
        this.InitAllCard();
        this.InitDeckPool();
    }

    private InitPlayer() {
        if (this.Player == null) {
            this.Player = new PlayerData();
        }
        this.Player.Init();
    }


    private InitAllCard() { 

        this.AllCardDict.Clear();
        for (let index = 0; index < RofManager.Singleton.CardTable.GetRowNum(); index++) {
            const element = RofManager.Singleton.CardTable.GetDataByRow(index);
            this.AllCardDict.Add(element.GetID(), new CardData(element.GetID()));
        }
    }



    private InitDeckPool() {

        for (let index = 0; index < RofManager.Singleton.DeckTable.GetRowNum(); index++) {
            const element = RofManager.Singleton.DeckTable.GetDataByRow(index);
            this.AllDeckDict.Add(element.GetID(),new DeckData(element.GetID()));
        }

        this.SetCurDeckById(this.AllDeckDict.Get(1).Config.GetID());
    }



    public SetCurDeckById(id: number) {

        if (GameData.Instance.CurDeck != null) {
            GameData.Instance.CurDeck.ClearUseData();
        }

        this.CurDeck = null;

   
        this.AllDeckDict.GetKeys().forEach(element => {
            if (element == id) {
                this.CurDeck = this.AllDeckDict.Get(element);
                return;
            }
        });
    }

    public Clear() {
        this.AllDeckDict.Clear();
    }

}
