import { CardData, CardType } from "./CardData";

import GameData from "./GameData";
import { UtilTool } from "../core/Assist/UtilTool";
import { RofDeckConfigRow } from "./ROF/RofElements";
import { RofManager } from "./ROF/RofManager";


export enum DeckCardType {
    Startup,
    Normal,
    Random,
    Over,
}

enum UnlockType {
    utMission,
    utAchievement,
}
class UnlockCond {
    public Type: UnlockType;
    public ID: string;

    constructor(type: UnlockType, id: string) {
        this.ID = id;
        this.Type = type;
    }
}

export class DeckData {

    //卡组ID
    public ID: number;

    //卡组配置
    public Config: RofDeckConfigRow;

    //当前抽卡次数
    private CurPickCount: number = 0;

    //随机卡牌池
    private RandomCardIdPool: number[] = [];

    //解锁条件
    public Unlock: UnlockCond[] = [];

    //解锁状态
    public HasUnlocked: boolean = false;

    public constructor(id: any) {
        this.ID = id;
        this.Config = RofManager.Singleton.DeckTable.GetDataByID(id);
        this.UnlockParse(this.Config.GetUnlockCond());

        var randCardArr = this.Config.GetRandomEventPool().split('|');

        for (let index = 0; index < randCardArr.length; index++) {
            const element = randCardArr[index];
            this.RandomCardIdPool.push(parseInt(element));
        }

    }

    private UnlockParse(cond: string) {
        //解析解锁条件(暂定每个卡组解锁条件只有一个)        
        if ("" == cond.trim() || undefined == cond) {
            //没有解锁条件默认解锁
            this.HasUnlocked = true;
        }

        let list = cond.split('-');
        if (list.length != 2) {
            //格式不符
            return
        }

        this.Unlock.push(new UnlockCond(Number(list[0]), list[1]));
        this.HasUnlocked = true;
    }


    public OnPickCard(): CardData {

        console.log("CurPickCountd:" + this.CurPickCount);

        if (this.CurPickCount >= this.Config.GetLimitCount()) {
            return null;
        }

        while (true) {

            var random = UtilTool.Random(0, this.RandomCardIdPool.length - 1);

            var pickId = this.RandomCardIdPool[random];

            var pickCard = GameData.Instance.AllCardDict.Get(pickId);

            console.log("抽取随机卡牌：" + pickCard.Config.GetID());

            if (pickCard.IsCanUse(GameData.Instance.CurTime)) {
                break;
            } 
        }

        this.CurPickCount++;

        return pickCard;
    }


    public ClearUseData() {
        this.CurPickCount = 0;
    }
}