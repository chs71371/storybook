import PlayerData, { CombatData } from "./PlayerData";
import { UtilTool } from "../core/Assist/UtilTool";
import { CardViewData } from "./CardView";
import BattleManage from "./BattleManage";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;


export class CombatViewData {

    public Data: CombatData = null;
    public CurHp: number;

    public constructor(data: CombatData) {
        this.Data = data;
        this.CurHp = data.HP;
    }
}


@ccclass
export default class CombatPanel extends cc.Component {

    @property(cc.Label)
    PlayerAtkText: cc.Label = null;

    @property(cc.Label)
    PlayerDefText: cc.Label = null;

    @property(cc.Node)
    PlayerHpImage: cc.Node = null;

    @property(cc.Label)
    EnemyAtkText: cc.Label = null;

    @property(cc.Label)
    EnemyDefText: cc.Label = null;

    @property(cc.Node)
    EnemyHpImage: cc.Node = null;

    @property(cc.Animation)
    PlayerImgAnimation: cc.Animation = null;

    @property(cc.Animation)
    EnemyImgAnimation: cc.Animation = null;

    PlayerCombatData: CombatViewData;

    EnemyCombatData: CombatViewData;

    protected mCombatSpeed: number = 10;

    protected mPlayerDamageRate: number = 0;

    protected mEnemyDamageRate: number = 0;

    protected mIsBattle: boolean = false;

    protected mIsOwnRound: boolean = true;

    protected mCurCardViewData: CardViewData;

    protected mHpImgWidth: number = 0;



    onLoad() {
        this.mHpImgWidth = this.PlayerHpImage.width;
    }

    public InitCombat(cardViewData: CardViewData) {

        this.mCurCardViewData = cardViewData;

        this.mIsBattle = true;

        var player = new CombatData();
        player.HP = 100;
        player.Atk = 20;
        player.Def = 5;
        var enemy = new CombatData();
        enemy.HP = 60;
        enemy.Atk = 8;
        enemy.Def = 5;

        this.PlayerCombatData = new CombatViewData(player);

        this.EnemyCombatData = new CombatViewData(enemy);

        this.EnemyImgAnimation.getComponent(cc.Sprite).spriteFrame = cardViewData.ContentImage;

        this.PlayerAtkText.string = "攻击力：" + this.PlayerCombatData.Data.Atk;
        this.PlayerDefText.string = "防御力：" + this.PlayerCombatData.Data.Def;

        this.EnemyAtkText.string = "攻击力：" + this.EnemyCombatData.Data.Atk;
        this.EnemyDefText.string = "防御力：" + this.EnemyCombatData.Data.Def;

        this.mPlayerDamageRate = Math.max(0, this.PlayerCombatData.Data.Atk - this.EnemyCombatData.Data.Def);
        this.mEnemyDamageRate = Math.max(0, this.EnemyCombatData.Data.Atk - this.PlayerCombatData.Data.Def);

        this.schedule(this.UpdateCombat, 0.8);
    }


    private UpdateCombat() {

        if (this.mIsOwnRound) {

            this.PlayerCombatData.CurHp -= this.mEnemyDamageRate;
            this.PlayerImgAnimation.play();

        } else {

            this.EnemyCombatData.CurHp -= this.mPlayerDamageRate;
            this.EnemyImgAnimation.play();
        }

        if (this.PlayerCombatData.CurHp <= 0) {

            this.CombatOver(false);
            return;
        }

        if (this.EnemyCombatData.CurHp <= 0) {

            this.CombatOver(true);
            return;
        }

        this.mIsOwnRound = !this.mIsOwnRound;

    }


    update(dt) {

        if (this.mIsBattle) {

            this.PlayerHpImage.width = UtilTool.Lerp(this.PlayerHpImage.width, this.mHpImgWidth * this.PlayerCombatData.CurHp / this.PlayerCombatData.Data.HP, dt * this.mCombatSpeed);

            this.EnemyHpImage.width = UtilTool.Lerp(this.EnemyHpImage.width, this.mHpImgWidth * this.EnemyCombatData.CurHp / this.EnemyCombatData.Data.HP, dt * this.mCombatSpeed);

        }
    }

    private CombatOver(isWin: boolean) {

        this.unschedule(this.UpdateCombat);

        this.scheduleOnce(() => { this.mIsBattle = false; BattleManage.Instance.battlePanel.SwitchStoryOrCombat(false); }, 1);
    }

}
