import BattlePanel from "./BattlePanel";
import { CardData, CardEffect, EffectType, CalcMethod, CardType } from "./CardData";
import GameData from "./GameData";
import { UtilTool } from "../core/Assist/UtilTool";
import PlayerData from "./PlayerData";
import GameMain from "./GameMain";
import { RofManager } from "./ROF/RofManager";
import { MissionStatus } from "./DataRecord";
import CardView, { CardViewData } from "./CardView";
import CombatPanel from "./CombatPanel";


const { ccclass, property } = cc._decorator;

@ccclass
export default class BattleManage extends cc.Component {

    public static Instance: BattleManage;

    @property(cc.Component)
    public battlePanel: BattlePanel = null;

    @property(cc.Component)
    public combatPanel: CombatPanel = null;

    //累积效果
    private mContinuousEffects: CardEffect[] = [];

    onLoad() {
        BattleManage.Instance = this;
    }

    public OnInit(tp: any) {
        switch (tp) {
            case "guide_01":
                this.battlePanel.OnBattleReady();
                break;
            case "guide_02":
                this.battlePanel.OnBattleReady();
                break;
        }
    }

 

    public OnActionUseCardEffect(cardViewData: CardViewData) {

        var cardData = cardViewData.Data;
        var isAgree = cardViewData.IsAgress;

        cardData.UseCard();

        switch (cardData.Config.GetType()) {
            case CardType.Back:
                this.StoryOver();
                return;
                break;
            default:
                GameData.Instance.CurTime++;
                this.ApplyEffect(this.mContinuousEffects, false);
                break;
        }

        if (isAgree) {
            this.ApplyEffect(cardData.AgreeEffectArr);

        } else {
            this.ApplyEffect(cardData.OppositionEffectArr);
        }



        //任务触发
        cardData.Missions.forEach(element => {
            //改变任务状态
            cc.log("Mission Status Changing");
            switch (GameMain.Instance.Record.ChangeMissionStatus(element)) {
                //界面变化            
                case MissionStatus.msDoing: {
                    break;
                }
                case MissionStatus.msDone: {
                    break;
                }
            }
        });

        //成就触发
        cardData.Achievements.forEach(element => {
            //达成成就
            cc.log("Achievements Done");
            if (GameMain.Instance.Record.AchievementDone(element)) {
                //界面变化
                //Todo:
            }
        });


        //连锁卡牌
        if (isAgree) {

            var nextCard = cardData.AgreeChainCard;
        } else {

            var nextCard = cardData.OppoChainCard;
        }


        this.FreshNextCard(nextCard);

    }



    private FreshNextCard(nextCardId: number) {

        console.log("nextCardId：" + nextCardId);

        if (nextCardId != 0) {
            this.battlePanel.FreshCardById(nextCardId);
        } else {

            var pickCard = GameData.Instance.CurDeck.OnPickCard();

            if (pickCard != null) {
                this.battlePanel.FreshCardById(pickCard.Config.GetID());
            } else {

                var nextDeck = GameData.Instance.CurDeck.Config.GetOverDeck();

                if (nextDeck != 0) {
                    GameData.Instance.SetCurDeckById(GameData.Instance.CurDeck.Config.GetOverDeck());
                    this.FreshNextCard(GameData.Instance.CurDeck.Config.GetStartupCardId());
                } else {
                    this.StoryOver();
                }
            }
        }
    }


    private ApplyEffect(effArr: CardEffect[], bNewEffect: boolean = true) {
        let Player = GameData.Instance.Player;
        if (null == Player) {
            return
        }

        for (let index = 0; index < effArr.length; index++) {
            const element = effArr[index];
            switch (element.Config.GetType()) {
                case EffectType.HpChange:
                    Player.Current.HP = this.CalcEffect(element.Config.GetCalculate(),
                        Player.Current.HP, element.Config.GetValue());
                    break;
                case EffectType.PropChange:
                    if (CalcMethod.AddSubstract == element.Config.GetCalculate()) {
                        let id = element.Config.GetItemID();
                        if (id == 0) {
                            break;
                        }

                        //道具数量为0对当前道具列表没有实际变化
                        let nCount = Number(element.Config.GetValue());
                        if (0 == nCount) {
                            break;
                        }

                        Player.Current.ChangeItem(id, nCount);
                    }
                    break;
            }

            //回合数判断
            element.CurRound--;
            if (element.CurRound > 0 && bNewEffect) {

                this.mContinuousEffects.push(element);

            } else if (element.CurRound == 0 && !bNewEffect) {

                this.mContinuousEffects.splice(this.mContinuousEffects.indexOf(element), 1);
            }
        }
    }

    private CalcEffect(type: CalcMethod, curValue: number, factor: any): number {
        var nRetValue = 0;
        switch (type) {
            case CalcMethod.AddSubstract:
                {
                    nRetValue = curValue + Number(factor);
                    break;
                }
            case CalcMethod.MultiplyDivide:
                {
                    nRetValue = curValue * (1 + Number(factor));
                    break;
                }
        }

        if (nRetValue == 0) {
            this.Die()
        }
        return nRetValue;
    }



    private Die() {
        this.battlePanel.Warn();
        //返回主菜单
        this.StoryOver();
    }


    public StoryOver() {
        GameData.Instance.Clear();
        GameMain.Instance.ReturnMainMenu();
    }
}
