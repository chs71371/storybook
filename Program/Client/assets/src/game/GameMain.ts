import GameData from "./GameData";
import BattleManage from "./BattleManage";
import { RofManager } from "./ROF/RofManager";
import { DataRecord } from "./DataRecord";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameMain extends cc.Component {

    public static Instance: GameMain;

    //数据记录器
    public Record : DataRecord = null;

    @property(cc.Node)
    UIRoot: cc.Node = null;

    @property(cc.Node)
    gamePlayView: cc.Node = null;

    @property(cc.Node)
    menuView: cc.Node = null;

    onLoad() {
        GameMain.Instance = this;
        RofManager.Singleton.init();

        if (null == this.Record)
        {
            this.Record = new DataRecord;
        }
    }

    start() {
        this.menuView.active = true;
        this.gamePlayView.active = false;
    }

    private Start_Lesson_01() {
        this.menuView.active = false;
        this.gamePlayView.active = true;

         GameData.Instance.InitData();


        BattleManage.Instance.OnInit("guide_01");
    }

    private Start_Lesson_02()
    {
        this.menuView.active = false;
        this.gamePlayView.active = true;
        GameData.Instance.InitData();
        BattleManage.Instance.OnInit("guide_02");
    }


    public ReturnMainMenu() {
        this.menuView.active = true;
        this.gamePlayView.active = false;
    }

    public LoadRes(thisObj: object, path: string, name: string, callback: Function) {
        if(path=="0")
        {
            return;
        }

        cc.loader.loadRes(path + name, function (err, res) {
            if (err) {
                cc.log(err);
            } else {
                callback(thisObj, res);
            }
        });
    }


}
