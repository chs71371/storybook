package rof
import (
"encoding/binary"
"math"
)
type sRofCardConfigRow struct {
mID int32
mType int32
mDescribe string
mShowImgUrl string
mAgreeDes string
mOppositionDes string
mAgreeEffectArr string
mOppositionEffectArr string
mCoolingMaxTime int32
mChainCard int32
mChainDeck int32
}
func (pOwn *sRofCardConfigRow) readBody(aBuffer []byte) int32 {
var nOffset int32
pOwn.mID = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mType = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
nDescribeLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mDescribe = string(aBuffer[nOffset:nOffset+nDescribeLen])
nOffset+=nDescribeLen
nShowImgUrlLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mShowImgUrl = string(aBuffer[nOffset:nOffset+nShowImgUrlLen])
nOffset+=nShowImgUrlLen
nAgreeDesLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mAgreeDes = string(aBuffer[nOffset:nOffset+nAgreeDesLen])
nOffset+=nAgreeDesLen
nOppositionDesLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mOppositionDes = string(aBuffer[nOffset:nOffset+nOppositionDesLen])
nOffset+=nOppositionDesLen
nAgreeEffectArrLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mAgreeEffectArr = string(aBuffer[nOffset:nOffset+nAgreeEffectArrLen])
nOffset+=nAgreeEffectArrLen
nOppositionEffectArrLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mOppositionEffectArr = string(aBuffer[nOffset:nOffset+nOppositionEffectArrLen])
nOffset+=nOppositionEffectArrLen
pOwn.mCoolingMaxTime = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mChainCard = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mChainDeck = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
return nOffset
}
func (pOwn *sRofCardConfigRow) GetID() int32 { return pOwn.mID } 
func (pOwn *sRofCardConfigRow) GetType() int32 { return pOwn.mType } 
func (pOwn *sRofCardConfigRow) GetDescribe() string { return pOwn.mDescribe } 
func (pOwn *sRofCardConfigRow) GetShowImgUrl() string { return pOwn.mShowImgUrl } 
func (pOwn *sRofCardConfigRow) GetAgreeDes() string { return pOwn.mAgreeDes } 
func (pOwn *sRofCardConfigRow) GetOppositionDes() string { return pOwn.mOppositionDes } 
func (pOwn *sRofCardConfigRow) GetAgreeEffectArr() string { return pOwn.mAgreeEffectArr } 
func (pOwn *sRofCardConfigRow) GetOppositionEffectArr() string { return pOwn.mOppositionEffectArr } 
func (pOwn *sRofCardConfigRow) GetCoolingMaxTime() int32 { return pOwn.mCoolingMaxTime } 
func (pOwn *sRofCardConfigRow) GetChainCard() int32 { return pOwn.mChainCard } 
func (pOwn *sRofCardConfigRow) GetChainDeck() int32 { return pOwn.mChainDeck } 
type sRofCardConfigTable struct { 
mRowNum int32
mColNum int32
mIDMap  map[int32]*sRofCardConfigRow
mRowMap map[int32]int32
}
func (pOwn *sRofCardConfigTable) newTypeObj() iRofRow {return new(sRofCardConfigRow)}
func (pOwn *sRofCardConfigTable) setRowNum(aNum int32) {pOwn.mRowNum = aNum}
func (pOwn *sRofCardConfigTable) setColNum(aNum int32) {pOwn.mColNum = aNum}
func (pOwn *sRofCardConfigTable) setIDMap(aKey int32, aValue iRofRow) {pOwn.mIDMap[aKey] = aValue.(*sRofCardConfigRow)}
func (pOwn *sRofCardConfigTable) setRowMap(aKey int32, aValue int32) {pOwn.mRowMap[aKey] = aValue}
func (pOwn *sRofCardConfigTable) init(aPath string) bool {
pOwn.mIDMap = make(map[int32]*sRofCardConfigRow)
pOwn.mRowMap = make(map[int32]int32)
return analysisRof(aPath, pOwn)
}
func (pOwn *sRofCardConfigTable) GetDataByID(aID int32) *sRofCardConfigRow {return pOwn.mIDMap[aID]}
func (pOwn *sRofCardConfigTable) GetDataByRow(aIndex int32) *sRofCardConfigRow {
nID, ok := pOwn.mRowMap[aIndex]
if ok == false {return nil}
return pOwn.mIDMap[nID]
}
func (pOwn *sRofCardConfigTable) GetRows() int32 {return pOwn.mRowNum}
func (pOwn *sRofCardConfigTable) GetCols() int32 {return pOwn.mColNum}
type sRofCardEffectConfigRow struct {
mID int32
mType int32
mValue string
mCalculate int32
mRoundCount int32
}
func (pOwn *sRofCardEffectConfigRow) readBody(aBuffer []byte) int32 {
var nOffset int32
pOwn.mID = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mType = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
nValueLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue = string(aBuffer[nOffset:nOffset+nValueLen])
nOffset+=nValueLen
pOwn.mCalculate = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mRoundCount = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
return nOffset
}
func (pOwn *sRofCardEffectConfigRow) GetID() int32 { return pOwn.mID } 
func (pOwn *sRofCardEffectConfigRow) GetType() int32 { return pOwn.mType } 
func (pOwn *sRofCardEffectConfigRow) GetValue() string { return pOwn.mValue } 
func (pOwn *sRofCardEffectConfigRow) GetCalculate() int32 { return pOwn.mCalculate } 
func (pOwn *sRofCardEffectConfigRow) GetRoundCount() int32 { return pOwn.mRoundCount } 
type sRofCardEffectConfigTable struct { 
mRowNum int32
mColNum int32
mIDMap  map[int32]*sRofCardEffectConfigRow
mRowMap map[int32]int32
}
func (pOwn *sRofCardEffectConfigTable) newTypeObj() iRofRow {return new(sRofCardEffectConfigRow)}
func (pOwn *sRofCardEffectConfigTable) setRowNum(aNum int32) {pOwn.mRowNum = aNum}
func (pOwn *sRofCardEffectConfigTable) setColNum(aNum int32) {pOwn.mColNum = aNum}
func (pOwn *sRofCardEffectConfigTable) setIDMap(aKey int32, aValue iRofRow) {pOwn.mIDMap[aKey] = aValue.(*sRofCardEffectConfigRow)}
func (pOwn *sRofCardEffectConfigTable) setRowMap(aKey int32, aValue int32) {pOwn.mRowMap[aKey] = aValue}
func (pOwn *sRofCardEffectConfigTable) init(aPath string) bool {
pOwn.mIDMap = make(map[int32]*sRofCardEffectConfigRow)
pOwn.mRowMap = make(map[int32]int32)
return analysisRof(aPath, pOwn)
}
func (pOwn *sRofCardEffectConfigTable) GetDataByID(aID int32) *sRofCardEffectConfigRow {return pOwn.mIDMap[aID]}
func (pOwn *sRofCardEffectConfigTable) GetDataByRow(aIndex int32) *sRofCardEffectConfigRow {
nID, ok := pOwn.mRowMap[aIndex]
if ok == false {return nil}
return pOwn.mIDMap[nID]
}
func (pOwn *sRofCardEffectConfigTable) GetRows() int32 {return pOwn.mRowNum}
func (pOwn *sRofCardEffectConfigTable) GetCols() int32 {return pOwn.mColNum}
type sRofDeckConfigRow struct {
mID int32
mLimitCount int32
mStartupCardId string
mOverCardId string
mCardPool string
}
func (pOwn *sRofDeckConfigRow) readBody(aBuffer []byte) int32 {
var nOffset int32
pOwn.mID = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mLimitCount = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
nStartupCardIdLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mStartupCardId = string(aBuffer[nOffset:nOffset+nStartupCardIdLen])
nOffset+=nStartupCardIdLen
nOverCardIdLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mOverCardId = string(aBuffer[nOffset:nOffset+nOverCardIdLen])
nOffset+=nOverCardIdLen
nCardPoolLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mCardPool = string(aBuffer[nOffset:nOffset+nCardPoolLen])
nOffset+=nCardPoolLen
return nOffset
}
func (pOwn *sRofDeckConfigRow) GetID() int32 { return pOwn.mID } 
func (pOwn *sRofDeckConfigRow) GetLimitCount() int32 { return pOwn.mLimitCount } 
func (pOwn *sRofDeckConfigRow) GetStartupCardId() string { return pOwn.mStartupCardId } 
func (pOwn *sRofDeckConfigRow) GetOverCardId() string { return pOwn.mOverCardId } 
func (pOwn *sRofDeckConfigRow) GetCardPool() string { return pOwn.mCardPool } 
type sRofDeckConfigTable struct { 
mRowNum int32
mColNum int32
mIDMap  map[int32]*sRofDeckConfigRow
mRowMap map[int32]int32
}
func (pOwn *sRofDeckConfigTable) newTypeObj() iRofRow {return new(sRofDeckConfigRow)}
func (pOwn *sRofDeckConfigTable) setRowNum(aNum int32) {pOwn.mRowNum = aNum}
func (pOwn *sRofDeckConfigTable) setColNum(aNum int32) {pOwn.mColNum = aNum}
func (pOwn *sRofDeckConfigTable) setIDMap(aKey int32, aValue iRofRow) {pOwn.mIDMap[aKey] = aValue.(*sRofDeckConfigRow)}
func (pOwn *sRofDeckConfigTable) setRowMap(aKey int32, aValue int32) {pOwn.mRowMap[aKey] = aValue}
func (pOwn *sRofDeckConfigTable) init(aPath string) bool {
pOwn.mIDMap = make(map[int32]*sRofDeckConfigRow)
pOwn.mRowMap = make(map[int32]int32)
return analysisRof(aPath, pOwn)
}
func (pOwn *sRofDeckConfigTable) GetDataByID(aID int32) *sRofDeckConfigRow {return pOwn.mIDMap[aID]}
func (pOwn *sRofDeckConfigTable) GetDataByRow(aIndex int32) *sRofDeckConfigRow {
nID, ok := pOwn.mRowMap[aIndex]
if ok == false {return nil}
return pOwn.mIDMap[nID]
}
func (pOwn *sRofDeckConfigTable) GetRows() int32 {return pOwn.mRowNum}
func (pOwn *sRofDeckConfigTable) GetCols() int32 {return pOwn.mColNum}
type sRofLanguageRow struct {
mID int32
mChinese string
mEnglish string
}
func (pOwn *sRofLanguageRow) readBody(aBuffer []byte) int32 {
var nOffset int32
pOwn.mID = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
nChineseLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mChinese = string(aBuffer[nOffset:nOffset+nChineseLen])
nOffset+=nChineseLen
nEnglishLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mEnglish = string(aBuffer[nOffset:nOffset+nEnglishLen])
nOffset+=nEnglishLen
return nOffset
}
func (pOwn *sRofLanguageRow) GetID() int32 { return pOwn.mID } 
func (pOwn *sRofLanguageRow) GetChinese() string { return pOwn.mChinese } 
func (pOwn *sRofLanguageRow) GetEnglish() string { return pOwn.mEnglish } 
type sRofLanguageTable struct { 
mRowNum int32
mColNum int32
mIDMap  map[int32]*sRofLanguageRow
mRowMap map[int32]int32
}
func (pOwn *sRofLanguageTable) newTypeObj() iRofRow {return new(sRofLanguageRow)}
func (pOwn *sRofLanguageTable) setRowNum(aNum int32) {pOwn.mRowNum = aNum}
func (pOwn *sRofLanguageTable) setColNum(aNum int32) {pOwn.mColNum = aNum}
func (pOwn *sRofLanguageTable) setIDMap(aKey int32, aValue iRofRow) {pOwn.mIDMap[aKey] = aValue.(*sRofLanguageRow)}
func (pOwn *sRofLanguageTable) setRowMap(aKey int32, aValue int32) {pOwn.mRowMap[aKey] = aValue}
func (pOwn *sRofLanguageTable) init(aPath string) bool {
pOwn.mIDMap = make(map[int32]*sRofLanguageRow)
pOwn.mRowMap = make(map[int32]int32)
return analysisRof(aPath, pOwn)
}
func (pOwn *sRofLanguageTable) GetDataByID(aID int32) *sRofLanguageRow {return pOwn.mIDMap[aID]}
func (pOwn *sRofLanguageTable) GetDataByRow(aIndex int32) *sRofLanguageRow {
nID, ok := pOwn.mRowMap[aIndex]
if ok == false {return nil}
return pOwn.mIDMap[nID]
}
func (pOwn *sRofLanguageTable) GetRows() int32 {return pOwn.mRowNum}
func (pOwn *sRofLanguageTable) GetCols() int32 {return pOwn.mColNum}
